defmodule Conjuntos do
  def interseccion(a, b) do
    Enum.filter(a, fn e -> e in b end)
  end
end
