defmodule ConjuntosTest do
  use ExUnit.Case
  doctest Conjuntos

  test "interseccion" do
    a = [1, 3, 5, 7]
    b = [1, 2, 3, 10]
    assert Conjuntos.interseccion(a, b) 
          |> MapSet.new == 
        MapSet.intersection(MapSet.new(a), MapSet.new(b))
  end

  test "diferencia" do
    a = [1, 3, 5, 7]
    b = [1, 2, 3, 10]
    assert Conjuntos.diferencia(a, b) 
          |> MapSet.new == 
        MapSet.difference(MapSet.new(a), MapSet.new(b))
  end

  test "union" do
    a = [1, 3, 5, 7]
    b = [1, 2, 3, 10]
    assert Conjuntos.union(a, b) 
          |> MapSet.new == 
        MapSet.union(MapSet.new(a), MapSet.new(b))
  end
end
